# yaq-aerotech

[![PyPI](https://img.shields.io/pypi/v/yaq-aerotech)](https://pypi.org/project/yaqd-aerotech)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaqd-aerotech)](https://anaconda.org/conda-forge/yaqd-aerotech)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.0M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaqd-aerotech/-/blob/master/CHANGELOG.md)

yaq daemon for Aerotech benchtop instruments using windows driver dlls

This package contains the following daemon(s):

- https://yaq.fyi/daemons/aerotech-ensemble