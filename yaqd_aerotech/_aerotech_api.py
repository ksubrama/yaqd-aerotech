import ctypes as c
import ctypes.wintypes as cw
import enum
import logging
import os.path
from typing import Any, List


# These enumerations and bit flags are obtained from EnsembleCommonStructures.h in the help file.
class AXISSTATUS(enum.IntFlag):
    # These are bit masks - 1 << n is 2 ** n.
  Enabled = 1 << 0
  Homed = 1 << 1
  InPosition = 1 << 2
  MoveActive = 1 << 3
  AccelerationPhase = 1 << 4
  DecelerationPhase = 1 << 5
  PositionCaptureActive = 1 << 6
  CurrentClamp = 1 << 7
  BrakeOutput = 1 << 8
  MotionIsCw = 1 << 9
  MasterSlaveControl = 1 << 10
  CalibrationActive = 1 << 11
  CalibrationEnabled = 1 << 12
  JoystickControl = 1 << 13
  Homing = 1 << 14
  MasterMotionSuppressed = 1 << 15
  GantryModeActive = 1 << 16
  GantryMasterActive = 1 << 17
  AutofocusActive = 1 << 18
  CommandShapingFilterDone = 1 << 19
  InPosition2 = 1 << 20
  ServoControl = 1 << 21
  CwEndOfTravelLimitInput = 1 << 22
  CcwEndOfTravelLimitInput = 1 << 23
  HomeLimitInput = 1 << 24
  MarkerInput = 1 << 25
  HallAInput = 1 << 26
  HallBInput = 1 << 27
  HallCInput = 1 << 28
  SineEncoderError = 1 << 29
  CosineEncoderError = 1 << 30
  EmergencyStopInput = 1 << 31


class STATUSITEM(enum.IntEnum):
  PositionCommand = 0
  PositionFeedback = 1
  PositionFeedbackAuxiliary = 2
  AxisStatus = 3
  AxisFault = 4
  AnalogInput0 = 5
  AnalogInput1 = 6
  AnalogOutput0 = 7
  AnalogOutput1 = 8
  DigitalInput0 = 9
  DigitalInput1 = 10
  DigitalInput2 = 11
  DigitalOutput0 = 12
  DigitalOutput1 = 13
  DigitalOutput2 = 14
  CurrentCommand = 15
  CurrentFeedback = 16
  AmplifierTemperature = 17
  DebugFlags = 18
  ProgramPositionCommand = 19
  ProgramCountTaskLibrary = 20
  ProgramCountTask1 = 21
  ProgramCountTask2 = 22
  ProgramCountTask3 = 23
  ProgramCountTask4 = 24
  PacketTime = 25
  ProgramPositionFeedback = 26
  AbsoluteFeedback = 27
  PlaneStatus0 = 28
  PlaneStatus1 = 29
  PlaneStatus2 = 30
  PlaneStatus3 = 31
  TaskState1 = 33
  TaskState2 = 34
  TaskState3 = 35
  TaskState4 = 36
  AnalogInput2 = 38
  AnalogInput3 = 39
  AnalogOutput2 = 40
  AnalogOutput3 = 41
  VelocityCommand = 42
  VelocityFeedback = 43
  AccelerationCommand = 44
  VelocityError = 45
  PositionError = 46
  CurrentError = 47
  AccelerationFeedback = 48
  AccelerationError = 49
  AnalogOutput4 = 50
  PiezoVoltageFeedback = 51
  PiezoVoltageCommand = 52


EnsembleHandleType = c.c_void_p


class Ensemble:

  def __init__(self, lib_path: str):
    self.lib_path = os.path.abspath(lib_path)
    assert os.path.exists(self.lib_path), 'Unable to find Aerotech dll directory.'
    self.en = self._load_dlls(self.lib_path)

  # Internal helpers
  def _load_dlls(self, lib_path):
    bin_path = os.path.join(lib_path, 'Bin64')
    c.WinDLL(os.path.join(bin_path, 'AeroBasic64.dll'))
    c.WinDLL(os.path.join(bin_path, 'EnsembleCore64.dll'))
    # the dll containing all the user-callable functions
    en = c.WinDLL(os.path.join(bin_path, 'EnsembleC64.dll'))
    # en.EnsembleConnect.argtypes = [c.POINTER(c.POINTER(c.c_void_p)), c.POINTER(cw.DWORD)]
    en.EnsembleConnect.restype = c.c_bool
    en.EnsembleGetLastErrorString.argtype = [cw.LPSTR, cw.DWORD]
    en.EnsembleGetLastErrorString.restype = c.c_bool
    en.EnsembleMotionSetupAbsolute.argtype = [c.c_void_p]
    en.EnsembleMotionSetupAbsolute.restype = c.c_bool
    en.EnsembleInformationGetAxisMask.argtype = [c.c_void_p, c.POINTER(c.c_uint)]
    en.EnsembleInformationGetAxisMask.restype = c.c_bool
    return en

  def _check(self, success: Any) -> Any:
    if success:
      return success
    else:
      cErrStr = c.create_string_buffer(100)
      # EnsembleGetLastErrorString returns 1 if an error string was successfully retrieved
      # (even if the error is {0} = no error)
      # This asserts that the error code was retrieved. Then we need to check what it actually is.
      assert (self.en.EnsembleGetLastErrorString(cErrStr, cw.DWORD(c.sizeof(cErrStr)))
              ), 'Cannot fetch ensemble error'
      logging.exception(bytes(cErrStr.raw).decode('base64', 'strict'))

  # Public interface functions.  The all check for success and raise exceptions otherwise.
  def Connect(self) -> List[EnsembleHandleType]:
    """Connect to all available Ensemble stages and returns a list of handles to those."""
    # Array of void *s to hold all enumerated stage handles.
    handles_p = c.pointer(c.c_void_p(0))
    handles_count = cw.DWORD(0)
    # Connect computer to all available ensemble stages
    self._check(self.en.EnsembleConnect(handles_p, c.pointer(handles_count)))
    logging.info(f'AerotechApi: {handles_count} stages connected.')
    if handles_count == 0:
      return []
    else:
      handles_array_p = c.cast(handles_p, c.POINTER(EnsembleHandleType * handles_count.value))
      return list(handles_array_p.contents)

  def Disconnect(self, handle: EnsembleHandleType) -> None:
    self._check(self.en.EnsembleDisconnect(handle))

  def InformationGetAxisMask(self, handle: EnsembleHandleType) -> int:
    """Returns the axis mask (bits 0-9 might be set if that axis is available)."""
    axis_mask = c.c_uint(0)
    self._check(self.en.EnsembleInformationGetAxisMask(handle, c.pointer(axis_mask)))
    return axis_mask.value

  def MotionSetupAbsolute(self, handle: EnsembleHandleType) -> None:
    self._check(self.en.EnsembleMotionSetupAbsolute(handle))

  def MotionEnable(self, handle: EnsembleHandleType, axis_mask: int) -> None:
    """Enables motion for each axis specified in the axis mask."""
    self._check(self.en.EnsembleMotionEnable(handle, c.c_uint(axis_mask)))

  def MotionMoveAbs(self, handle: EnsembleHandleType, axis_mask: int, position: float, speed: float) -> None:
    pos_p = c.pointer(c.c_double(position))
    sp_p = c.pointer(c.c_double(speed))
    self._check(self.en.EnsembleMotionMoveAbs(handle, c.c_uint(axis_mask), pos_p, sp_p))

  def MotionFreeRun(self, handle: EnsembleHandleType, axis_mask: int, speed: float) -> None:
    sp_p = c.pointer(c.c_double(speed))
    self._check(self.en.EnsembleMotionFreeRun(handle, c.c_uint(axis_mask), sp_p))

  def StatusGet_AxisStatus(self, handle: EnsembleHandleType, axis: int) -> AXISSTATUS:
    value = c.c_ulonglong(0)
    item = cw.DWORD(STATUSITEM.AxisStatus)
    self._check(self.en.EnsembleStatusGetItem(handle, c.c_uint(axis), item, c.pointer(value)))
    return AXISSTATUS(value.value)

  def StatusGetItem(self, handle: EnsembleHandleType, axis: int, item: STATUSITEM) -> float:
    """Reads an axis status value for the specified axis.
    Only use this if the status item you want is a floating point double value.
    Otherwise use a specialized StatusGet_* function to get values of the correct type.
    """
    value = c.c_double(0)
    self._check(self.en.EnsembleStatusGetItem(handle, c.c_uint(axis), cw.DWORD(item), c.pointer(value)))
    return value.value
