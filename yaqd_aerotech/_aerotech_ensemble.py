__all__ = ["AerotechEnsemble"]

import asyncio
import enum
import logging
import pathlib
from typing import Dict, Any
from yaqd_core import IsHomeable, HasLimits, HasPosition, IsDaemon
import _aerotech_api as aero


class AeroLim(enum.Enum):
  UNKNOWN = 1
  GOING_LEFT = 2
  GOING_RIGHT = 3
  HOMING = 4
  COMPLETE = 5


class AerotechEnsemble(IsHomeable, HasLimits, HasPosition, IsDaemon):
  _kind = "aerotech-ensemble"

  def __init__(self, name: str, config: Dict[str, Any], config_filepath: pathlib.Path):
    super().__init__(name, config, config_filepath)
    self._en = aero.Ensemble(config['lib_path'])

    handles = self._en.Connect()
    assert len(handles) == 1, 'Multiple stages detected - we only handle one.'
    self._handle = handles[0]

    # Reset the controller?
    # self._en.Reset(self._handle, c.c_bool(False))
    self._axis = 0
    axis_mask = self._en.InformationGetAxisMask(self._handle)
    assert axis_mask == 1, 'We only handle one linear axis right now.'
    self._axis_mask = 1

    # EnsembleMotionSetupAbsolute tells the stage we will be using absolute coordinates
    # rather than relative/incremental coordinates for motion.
    self._en.MotionSetupAbsolute(self._handle)
    self._en.MotionEnable(self._handle, self._axis_mask)

    # This is how many mm to add/subtract from the limits in order to not test the limits of the stage.
    # The stage will fly (with different configured velocitiess) to different limits based on how far
    # the linear translation motots are able to throw the stage before they get stuck.  This means the
    # limits will change with every restart and with the initial position and weight of the stage,
    # This margin is to minimize this variance.
    self._limit_margin = config['limit_margin']
    self._state['velocity'] = config['velocity']
    self._phys_limit_left = None
    self._phys_limit_right = None
    self._limit_discovery = AeroLim.UNKNOWN

  def close(self) -> None:
    self._en.Disconnect(self._handle)

  def home(self) -> None:
    # This is not the home maintained by the ensemble controller.  We maintain a "home" which is the
    # left limit value and handle all offset translation ourselves.
    self.set_position(0.0)

  def _set_position(self, position: float) -> None:
    assert self._phys_limit_left is not None, 'Only call this function after we gave callibrated limits.'
    self._en.MotionMoveAbs(self._handle, self._axis_mask, position + self._phys_limit_left,
                           abs(self._state['velocity']))
    # set_position automatically sets the busy signal.

  def travelling(self) -> bool:
    return bool(self._en.StatusGet_AxisStatus(self._handle, self._axis) & aero.AXISSTATUS.MoveActive)

  def _get_phys_position(self) -> float:
    return self._en.StatusGetItem(self._handle, self._axis, aero.STATUSITEM.ProgramPositionFeedback)

  def _discover_limits(self) -> None:
    assert self._limit_discovery != AeroLim.COMPLETE, 'Only call this function when we are callibrating limits.'
    if self.travelling():
      return

    if self._limit_discovery == AeroLim.UNKNOWN:
      logging.info('Free-running stage to the right.')
      self._limit_discovery = AeroLim.GOING_RIGHT
      self._en.MotionFreeRun(self._handle, self._axis_mask, abs(self._state['velocity']))
    elif self._limit_discovery == AeroLim.GOING_RIGHT:
      logging.info('Right limit reached.  Free-running stage to the to the left.')
      self._phys_limit_right = self._get_phys_position() - self._limit_margin
      logging.info(f'Right limit in physical units is: {self._phys_limit_right} mm')
      self._limit_discovery = AeroLim.GOING_LEFT
      self._en.MotionFreeRun(self._handle, self._axis_mask, -abs(self._state['velocity']))
    elif self._limit_discovery == AeroLim.GOING_LEFT:
      logging.info('Left limit reached.  Stage limit discovery complete.')
      self._phys_limit_left = self._get_phys_position() + self._limit_margin
      logging.info(f'Left limit in physical units is: {self._phys_limit_left} mm')
      assert self._phys_limit_right is not None
      assert self._phys_limit_left < self._phys_limit_right
      # Actual travel limits - also in logical units.
      self._state['hw_limits'] = (0.0, self._phys_limit_right - self._phys_limit_left)
      self._limit_discovery = AeroLim.HOMING
      self._set_position(0.0)
    elif self._limit_discovery == AeroLim.HOMING:
      self._state['position'] = 0.0
      self._state['destination'] = 0.0
      self._limit_discovery = AeroLim.COMPLETE
      self._busy = False

  async def update_state(self) -> None:
    """Continually monitor and update the current daemon state."""
    while self._limit_discovery != AeroLim.COMPLETE:
      self._busy = True
      self._discover_limits()
      await asyncio.sleep(0.01)

    while True:
      # Perform any updates to internal state
      assert self._phys_limit_left is not None
      self._state['position'] = self._get_phys_position() - self._phys_limit_left

      # There must be at least one `await` in this loop
      if self._busy:
        # This is horrible.  I need to replace this with a proper condition
        # variable that broadcasts when a motion has been commanded and again when
        # one completes.
        delta = abs(self._state['destination'] - self._state['position'])
        self._busy = delta > 0.01 or self.travelling()
        await asyncio.sleep(0.01)
      else:
        await self._busy_sig.wait()


# __name__ is an internal variable. Basically this checks if this script is invoked directly rather than imported from
# another module
if __name__ == "__main__":
  AerotechEnsemble.main()  # 'main' is inherited from the daemon class.
